<?php

namespace getinstance\api_util\util;

class PubSubManager
{
    private array $callbacks = [];

    public function listen(string $key, callable $callback)
    {
        $this->callbacks[$key] ??= [];
        $this->callbacks[$key][] = $callback;
    }

    public function announce(string $key, array $data)
    {
        $this->callbacks[$key] ??= [];
        foreach ($this->callbacks[$key] as $callback) {
            $callback($data);
        }
    }
}
