<?php

declare(strict_types=1);

namespace getinstance\api_util\util;

/**
 * A simple callback registry.
 */
final class Callback
{
    /**
     * @var array<string, callable[]>
     */
    private static array $filterLookup = [];

    private function __construct()
    {
    }

    /**
     * Performs all registered callbacks for the given action.
     * @param string $name The action name.
     * @param mixed $args The arguments to pass to the callbacks.
     */
    public static function doAction(string $name, ...$args): void
    {
        if (!isset(self::$filterLookup[$name])) {
            return;
        }

        foreach (self::$filterLookup[$name] as $filter) {
            $filter(...$args);
        }
    }

    /**
     * Performs all registered callbacks for the given filter.
     *
     * @template TValue The type of the value to filter.
     *
     * @param string $name The filter name.
     * @param TValue $result The value on which the filters should be applied.
     * @param mixed $args The arguments to pass to the callbacks.
     *
     * @return TValue The filtered value.
     */
    public static function doFilter(string $name, $result, ...$args): mixed
    {
        if (!isset(self::$filterLookup[$name])) {
            return $result;
        }

        foreach (self::$filterLookup[$name] as $filter) {
            $result = $filter($result, ...$args);
        }

        return $result;
    }

    /**
     * Registers a callback for the given action / filter.
     *
     * @param string $name The action / filter name.
     * @param callable $callable The callback to register.
     */
    public static function register(string $name, callable $callable): void
    {
        if (!isset(self::$filterLookup[$name])) {
            self::$filterLookup[$name] = [];
        }

        self::$filterLookup[$name][] = $callable;
    }
}
