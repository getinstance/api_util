<?php

/*
 * This file is part of the getinstance/api_util framework.
 *
 * (c)2018 getInstance Ltd <matt@getinstance.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */


namespace getinstance\api_util\util;

use getinstance\api_util\controller\FrontController;
use Slim\Psr7\Request as SlimRequest;
use Slim\Psr7\Uri;
use Slim\Psr7\Factory\StreamFactory;
use Slim\Psr7\Headers;
use Slim\Psr7\Factory\ServerRequestFactory;

//use Symfony\Component\HttpKernel\Client;
//use Symfony\Component\HttpKernel\Client;
//use Symfony\Component\BrowserKit\Client;
//use Symfony\Component\BrowserKit\Response;
//use Symfony\Component\HttpFoundation\Request;

//use Symfony\Component\HttpFoundation\Response;

// extends Client

class ApiUtilClient
{
    protected $controller;

    public function __construct(FrontController $controller, $server = array())
    {
        $this->controller = $controller;
        //parent::__construct($server);
    }

    public function createJsonRequest(
        string $method,
        string $path,
        array $headers = [],
        array $cookies = [],
        array $data = [],
        //?string $json = null,
    ): SlimRequest {
        $request = $this->createRequest($method, $path, $headers, $cookies);
        $request->getBody()->write((string)json_encode($data));
        $request = $request->withHeader('Content-Type', 'application/json');
        return $request;
    }

    public function createRequest(
        string $method,
        string $path,
        array $headers = [],
        array $cookies = [],
        array $serverParams = []
    ): SlimRequest {

        $factory = new ServerRequestFactory();
        $request = $factory->createServerRequest($method, $path, $serverParams);
        foreach ($headers as $name => $value) {
            $request = $request->withHeader($name, $value);
        }
        $request = $request->withCookieParams($cookies);
        return $request;
    }
}
