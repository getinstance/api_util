<?php

/*
 * This file is part of the getinstance/api_util framework.
 *
 * (c)2018 getInstance Ltd <matt@getinstance.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */



namespace getinstance\api_util\util;

use PHPUnit\Framework\TestCase;
use getinstance\api_util\controller\FrontController;
use getinstance\api_util\controller\InitWareInterface;

abstract class WebTestCase extends TestCase
{
    protected ?ApiUtilClient $client = null;
    protected ?FrontController $controller = null;

    /**
     * PHPUnit setUp for setting up the application.
     *
     * Note: Child classes that define a setUp method must call
     * parent::setUp().
     */
    protected function setUp(): void
    {
        $this->createController();
    }

    private function createController(): void
    {
        if (!is_null($this->controller)) {
            return;
        }

        $fc = new FrontController();
        $fc->addInitWare($this->getInitWare());
        $fc->init(['confpath' => $this->getConfPath(), 'confname' => $this->getConfName()]);

        $this->controller = $fc;
        $this->client = $this->createClient();
    }

    abstract public function getConfPath(): string;
    abstract public function getConfName(): string;
    abstract public function getInitWare(): InitWareInterface;


    /**
     * Creates a Client.
     *
     * @param array $server Server parameters
     *
     * @return ApiUtilClient A Client instance
     */
    public function createClient(array $server = []): ApiUtilClient
    {
        return new ApiUtilClient($this->controller, $server);
    }

    public function put($url, $fields = [], $expected = 201)
    {
        $files = [];
        $extraHeaders = [];
        return $this->requestJson('PUT', $url, $fields, $files, $extraHeaders);
    }

    public function post($url, $fields = [], $expected = 201)
    {
        $files = [];
        $extraHeaders = [];
        return $this->requestJson('POST', $url, $fields, $files, $extraHeaders);
    }

    public function get($url, $fields = [], $expected = 200)
    {
        $files = [];
        $extraHeaders = [];
        //$querystring = http_build_query($fields);
        //$url = $url.?.$querystring;
        return $this->requestJson('GET', $url, $fields, $files, $extraHeaders);
    }


    private function requestJson($method, $uri, $fields, $files, $extraHeaders)
    {
        $basics = [
            'Content-Type' => 'application/json',
            'HTTP_ACCEPT' => 'application/json'
        ];

        $data = json_encode($fields);
        $request = $this->client->createJsonRequest(
            $method,
            $uri,
            array_merge($basics, $extraHeaders),
            [],
            $fields
        );
        $app = $this->controller->getApp();
        $response = $app->handle($request);
        $content = $response->getBody();
        $status = $response->getStatusCode();
        if ($content) {
            $content = json_decode($content, true);
        }
        if ($status == 500) {
            print_r($content);
        }
        return [$status, $content, $response];
    }
}
