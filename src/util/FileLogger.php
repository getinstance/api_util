<?php

namespace getinstance\api_util\util;

use InvalidArgumentException;
use Psr\Log\LoggerInterface;
use Psr\Log\LogLevel;
use Stringable;

class FileLogger implements LoggerInterface
{
    private int $logLevel;
    private string $path;

    public function __construct(string $path = '', string $logLevel = 'debug')
    {
        $this->path = $path === '' ? ini_get('error_log') : $path;
        $this->logLevel = self::resolveLogLevel($logLevel);
    }

    #region implements LoggerInterface

    public function emergency(string|Stringable $message, array $context = []): void
    {
        $this->writeLog(LogLevel::EMERGENCY, (string)$message, $context);
    }

    public function alert(string|Stringable $message, array $context = []): void
    {
        $this->writeLog(LogLevel::ALERT, (string)$message, $context);
    }

    public function critical(string|Stringable $message, array $context = []): void
    {
        $this->writeLog(LogLevel::CRITICAL, (string)$message, $context);
    }

    public function error(string|Stringable $message, array $context = []): void
    {
        $this->writeLog(LogLevel::ERROR, (string)$message, $context);
    }

    public function warning(string|Stringable $message, array $context = []): void
    {
        $this->writeLog(LogLevel::WARNING, (string)$message, $context);
    }

    public function notice(string|Stringable $message, array $context = []): void
    {
        $this->writeLog(LogLevel::NOTICE, (string)$message, $context);
    }

    public function info(string|Stringable $message, array $context = []): void
    {
        $this->writeLog(LogLevel::INFO, (string)$message, $context);
    }

    public function debug(string|Stringable $message, array $context = []): void
    {
        $this->writeLog(LogLevel::DEBUG, (string)$message, $context);
    }

    public function log($level, string|Stringable $message, array $context = []): void
    {
        $this->writeLog($level, (string)$message, $context);
    }

    #endregion

    protected function writeLog(string $level, string $message, array $context = []): void
    {
        if (self::resolveLogLevel($level) > $this->logLevel) {
            return;
        }

        $log = sprintf('[%s] %s: %s', gmdate('Y-m-d H:i:s'), strtoupper($level), rtrim($message));
        if (!empty($context)) {
            $log .= "\nLog context:\n" . print_r($context, true);
        }

        $lines = explode("\n", $log);
        for ($i = 1; $i < count($lines); $i++) {
            $lines[$i] = '    ' . $lines[$i];
        }
        $log = implode("\n", $lines) . "\n";

        file_put_contents($this->path, $log, FILE_APPEND);
    }

    private static function resolveLogLevel(string $logLevel): int
    {
        return match ($logLevel) {
            LogLevel::EMERGENCY => 1,
            LogLevel::ALERT => 2,
            LogLevel::CRITICAL => 3,
            LogLevel::ERROR => 4,
            LogLevel::WARNING => 5,
            LogLevel::NOTICE => 6,
            LogLevel::INFO => 7,
            LogLevel::DEBUG => 8,
            default => throw new InvalidArgumentException("Invalid log level: $logLevel"),
        };
    }
}
