<?php

/*
 * This file is part of the getinstance/api_util framework.
 *
 * (c)2018 getInstance Ltd <matt@getinstance.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace getinstance\api_util\command;

use getinstance\api_util\command\Command;
use Psr\Http\Message\ServerRequestInterface as Request;

class PostBinariesCommand extends Command
{
    public function doThumb($dir, $name, $ext, $sizemodifier = "thumb", $newwidth = 40)
    {
        $path = "{$dir}/{$name}.{$ext}";
        $thumbpath = "{$dir}/{$name}_{$sizemodifier}.{$ext}";

        $funcend = ($ext == "jpg") ? "jpeg" : $ext;
        list($width, $height) = getimagesize($path);
        $createfunc = "imagecreatefrom" . $funcend;
        $savefunc = "image" . $funcend;

        $percentage = ($newwidth / $width) * 100;
        $newheight = ($percentage / 100) * $height;

        $source = $createfunc($path);
        $thumb = imagecreatetruecolor($newwidth, $newheight);
        imagecopyresized($thumb, $source, 0, 0, 0, 0, $newwidth, $newheight, $width, $height);
        $savefunc($thumb, $thumbpath);
    }

    protected function doExecute(Request $request)
    {
        $srequest = $this->psr7toSymfonyRequest($request);
        $keys = $srequest->files->keys();
        $conf = $this->conf;

        $bindir = $conf->get("bindir");
        $binurl = $conf->get("binurl");
        $binhost = $conf->get("binhost");

        $bindir = realpath($bindir);
        if (!is_dir($bindir) || !is_writeable($bindir)) {
            throw new \Exception("'$bindir' either does not exist or is not a writeable directory");
        }

        $ret = [];
        $thumbable = ['png', 'jpg', 'jpeg', 'gif'];

        foreach ($keys as $key) {
            $fileobj = $request->files->get($key);
            $ext = $fileobj->getClientOriginalExtension();
            $mime = $fileobj->getMimeType();

            // make sure target does not exist
            do {
                $core = uniqid();
                $name = $core . "." . $ext;
                $targetobj = new \SplFileInfo("{$bindir}/{$name}");
            } while ($targetobj->isFile());

            $url = "{$binhost}/{$binurl}/{$name}";
            $url = preg_replace("|/+|", "/", $url);
            $fileobj->move($bindir, $name);
            if (in_array($ext, $thumbable)) {
                //error_log("attempting to do thumb");
                $this->doThumb($bindir, $core, $ext);
                $this->doThumb($bindir, $core, $ext, "med", 300);
            }
            $ret[] = [
                "original" => $key,
                "name" => $name,
                "mime" => $mime,
                "url" => "://{$url}"
            ];
        }
        return [$ret, 201];
    }
}
