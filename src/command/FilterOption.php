<?php

namespace getinstance\api_util\command;

use InvalidArgumentException;

/**
 * Represents a filter option, e.g. tag=abc
 */
class FilterOption
{
    /**
     * A list of valid operators
     */
    public const VALID_OPERATORS = ['=', '~', '>', '<', '>=', '<=', '!=', 'NOT IN', "IN"];

    /**
     * The name of the field to filter on
     */
    public string $field;

    /**
     * The operator to use, one of: =, ~, >, <, >=, <=, !=, NOT IN
     */
    public string $operator;

    /**
     * The value to filter on
     */
    public string $value;

    /**
     * Constructor
     *
     * @param string $field The name of the field to filter on.
     * @param string $operator The operator to use.
     * @param string $value The value to filter on.
     */
    public function __construct(string $field, string $operator, string $value)
    {
        $this->field = $field;

        if (!in_array($operator, self::VALID_OPERATORS, true)) {
            throw new InvalidArgumentException("Invalid operator: $operator");
        }

        $this->operator = $operator;
        $this->value = $value;
    }

    /**
     * Parses a filter option from a string.
     *
     * @param string $arg The string to parse.
     *
     * @return FilterOption The parsed filter option.
     */
    public static function parse(string $arg): self
    {
        $opRegex = implode('|', self::VALID_OPERATORS);
        $regex = '/^\s*(\w[\w\d_]*)\s*(' . $opRegex . ')\s*(.*)$/';
        if (!preg_match($regex, $arg, $matches)) {
            throw new InvalidArgumentException("Unable to parse: $arg");
        }

        return new self($matches[1], $matches[2], rtrim($matches[3]));
    }
}
