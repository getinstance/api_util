<?php

/*
 * This file is part of the getinstance/api_util framework.
 *
 * (c)2018 getInstance Ltd <matt@getinstance.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace getinstance\api_util\command;

use getinstance\api_util\controller\Conf;
use getinstance\api_util\util\PubSubManager;
use Symfony\Component\HttpFoundation\Request as sRequest;
use Symfony\Component\HttpFoundation\Response as sResponse;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use Slim\Psr7\Factory\ServerRequestFactory as Psr17Factory;
use Slim\Psr7\Factory\StreamFactory;
use Slim\Psr7\Factory\UploadedFileFactory;
use Slim\Psr7\Factory\ResponseFactory;
use Symfony\Bridge\PsrHttpMessage\Factory\PsrHttpFactory;
use Symfony\Bridge\PsrHttpMessage\Factory\HttpFoundationFactory;
use Slim\Views\Twig;

abstract class Command
{
    protected readonly Conf $conf;
    protected readonly PubSubManager $pubSubManager;

    public function __construct(Conf $conf, PubSubManager $pubSubManager)
    {
        $this->conf = $conf;
        $this->pubSubManager = $pubSubManager;
    }

    public function getPubSubManager(): PubSubManager
    {
        return $this->pubSubManager;
    }

    public function symfonyToPsr7Response(sResponse $sresp): Response
    {
        $psr17Factory = new Psr17Factory();
        $streamfactory = new StreamFactory();
        $uploadedfilefactory = new UploadedFileFactory();
        $responsefactory = new ResponseFactory();
        $psrHttpFactory = new PsrHttpFactory($psr17Factory, $streamfactory, $uploadedfilefactory, $responsefactory);
        $psrResponse = $psrHttpFactory->createResponse($sresp);
        return $psrResponse;
    }

    public function symfonyToPsr7Request(sRequest $sreq): Request
    {
        $psr17Factory = new Psr17Factory();
        $streamfactory = new StreamFactory();
        $uploadedfilefactory = new UploadedFileFactory();
        $responsefactory = new ResponseFactory();
        $psrHttpFactory = new PsrHttpFactory($psr17Factory, $streamfactory, $uploadedfilefactory, $responsefactory);
        $psrRequest = $psrHttpFactory->createRequest($sreq);
        return $psrRequest;
    }

    public function psr7toSymfonyRequest(Request $req): sRequest
    {
        $httpFoundationFactory = new HttpFoundationFactory();
        $symfonyRequest = $httpFoundationFactory->createRequest($req);
        return $symfonyRequest;
    }

    public function psr7toSymfonyResponse(Response $resp): sResponse
    {
        $httpFoundationFactory = new HttpFoundationFactory();
        $symfonyResponse = $httpFoundationFactory->createResponse($resp);
        return $symfonyResponse;
    }

    public function twigResponse(Request $request, $template, array $vals, ?Response $response = null): Response
    {
        $response = $this->makeResponse($response);
        $view = Twig::fromRequest($request);
        return $view->render($response, $template, $vals);
    }

    private function makeResponse(?Response $response = null): Response
    {
        if (is_null($response)) {
            $responsefactory = new ResponseFactory();
            $response = $responsefactory->createResponse();
        }
        return $response;
    }

    public function requestGet(Request $request, string $key): mixed
    {
        $queryvals = $request->getQueryParams() ?? [];
        return ($queryvals[$key] ?? null);
    }

    public function requestPost(Request $request, string $key): mixed
    {
        $bodyvals = $request->getParsedBody() ?? [];
        return ($bodyvals[$key] ?? null);
    }

    public function allRequestParams(Request $request): array
    {
        $bodyvals = $request->getParsedBody() ?? [];
        $queryvals = $request->getQueryParams() ?? [];
        return array_merge($queryvals, $bodyvals);
    }

    public function jsonResponse(Request $request, array $resultandcode, ?Response $response = null): Response
    {
        $response = $this->makeResponse($response);
        list($data, $code) = $resultandcode;
        $payload = json_encode($data);
        $response->getBody()->write($payload);
        return $response
            ->withHeader('Content-Type', 'application/json')
            ->withStatus($code);
    }

    public function redirectResponse($path, ?Response $response = null): Response
    {
        //print "path: $path";
        return $this->makeResponse($response)
            ->withHeader('Location', $path)
            ->withStatus(302);
    }

    /**
     * Extracts filter options from the incoming request.
     *
     * @param Request $request The incoming request.
     * @param bool    $strictMode If true, the result will be an array of FilterOption objects.
     *
     * @return array<string, string|string[]>|array<FilterOption> The filter options.
     */
    public function getFilterArray(Request $request, bool $strictMode = false): array
    {
        /** @var FilterOption[] $filters */
        $filters = [];
        $params = $this->allRequestParams($request);
        $clauses = explode(';', $params['filter'] ?? '');
        foreach ($clauses as $clause) {
            if ($clause === '' || ctype_space($clause)) {
                continue;
            }
            $filters[] = FilterOption::parse($clause);
        }

        if ($strictMode) {
            return $filters;
        }

        $oldFilters = [];
        foreach ($filters as $filter) {
            if ($filter->operator === '=') {
                $oldFilters[$filter->field] = $filter->value;
            } else {
                $oldFilters[$filter->field][$filter->operator] = $filter->value;
            }
        }

        return $oldFilters;
    }

    protected function doOrder(Request $request)
    {
        $queryvals = $request->getQueryParams() ?? [];
        if (!empty($queryvals['order']) && is_array($queryvals['order'])) {
            return $queryvals['order'];
        }
        $orderstr = $queryvals['order'] ?? "";
        $orderarray = ($orderstr) ? explode(",", $orderstr) : [];
        $order = [];
        foreach ($orderarray as $key => $val) {
            if (preg_match("/^(.*)\|(.*)$/", $val, $matches)) {
                $order[$matches[1]] = strtoupper($matches[2]);
            } else {
                $order[$val] = "ASC";
            }
        }
        return $order;
    }

    protected function doRequired(Request $request, array $required, $rejectempty = false)
    {
        $row = [];
        $params = $this->allRequestParams($request);
        foreach ($required as $field) {
            $val = $params[$field] ?? null;
            if (is_null($val)) {
                throw new \Exception("'$field' is required", 400);
            }
            if ($rejectempty && empty($val)) {
                throw new \Exception("'$field' is required (no empties)", 400);
            }
            $row[$field] = $val;
        }
        return $row;
    }
}
