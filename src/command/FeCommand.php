<?php

/*
 * This file is part of the getinstance/api_util framework.
 *
 * (c)2018 getInstance Ltd <matt@getinstance.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace getinstance\api_util\command;

use getinstance\api_util\controller\Conf;
use getinstance\api_util\util\PubSubManager;
use Odan\Session\SessionInterface;

class FeCommand extends Command
{
    protected SessionInterface $session;

    public function __construct(Conf $conf, PubSubManager $pubSubManager, SessionInterface $session)
    {
        parent::__construct($conf, $pubSubManager);
        $this->session = $session;
    }

    public function redirect($url, $errormsg = null, $infomsg = null, $args = [])
    {
        $this->messagesForRedirect($errormsg, $infomsg);
        $this->argsForRedirect($args);
        return $this->redirectResponse($url);
    }

    protected function argsForRedirect($args = [])
    {
        foreach ($args as $argkey => $argval) {
            $this->session->set("redirect.{$argkey}", $argval);
        }
    }

    protected function messagesForRedirect($errormsg = null, $infomsg = null)
    {
        $this->session->set("errormsg", $errormsg);
        $this->session->set("infomsg", $infomsg);
    }

    protected function getAndClearRedirectVals()
    {
        $session = $this->session;
        $vals = [];
        foreach (["infomsg", "errormsg"] as $label) {
            if (is_string($msg = $session->get($label))) {
                $session->set($label, null);
                $vals[$label] = $msg;
            }
        }
        $all = $session->all();
        foreach ($all as $key => $val) {
            if (preg_match("/^redirect\\.(.*)$/", $key, $matches)) {
                $vals[$matches[1]] = $val;
                $session->delete($key, null);
            }
        }
        return $vals;
    }
}
