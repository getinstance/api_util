<?php

/*
 * This file is part of the getinstance/api_util framework.
 *
 * (c)2018 getInstance Ltd <matt@getinstance.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */


namespace getinstance\api_util\model;

//use getinstance\commons\model\ModelRecord as CommonsRecord;

abstract class ModelRecord
{
    private $fields;

    public function __construct(array $row, $validate = true)
    {
        $this->fields = $row;
        if ($validate) {
            $this->validateConstructorArray();
        }
    }

    public function validateConstructorArray()
    {
        foreach ($this->requiredFields() as $field) {
            if (! array_key_exists($field, $this->fields)) {
                throw new \Exception("required field '{$field}' missing for " . get_class($this));
            }
        }

        foreach ($this->fields as $field => $val) {
            if (method_exists($this, "set{$field}")) {
                return call_user_func([$this, "set{$field}"], $val);
            }
        }
    }

    public function isNull()
    {
        return false;
    }

    public function fieldExists($key)
    {
        if (array_key_exists($key, $this->fields)) {
            return true;
        }
        return false;
    }

    public function fieldIs($key, $val)
    {
        if (! $this->fieldExists($key)) {
            return false;
        }
        return ($this->getField($key) == $val);
    }

    public function truthy($key)
    {
        if (! $this->fieldExists($key)) {
            return false;
        }
        if (empty($this->fields[$key])) {
            return false;
        }
        return true;
    }

    protected function forceSetField($key, $val)
    {
        return $this->fields[$key] = $val;
    }

    public function setField($key, $val, $strict = true)
    {
        $fieldkeys = $this->entityFields();
        if ((! in_array($key, $fieldkeys)) && $strict) {
            throw new \Exception("attempt to set unknown field: '$key'");
        }
        // if the child class is enforcing type with a setter - call it
        if (method_exists($this, "set{$key}")) {
            return call_user_func([$this, "set{$key}"], $val);
        } else {
            //error_log("setting field $key to $val");
            return $this->fields[$key] = $val;
        }
    }

    public function getField($key, $default = null)
    {
        if (! is_null($default) && ! $this->truthy($key)) {
            return $default;
        }
        if (! array_key_exists($key, $this->fields)) {
            return null;
        }
        return $this->fields[$key];
    }

    public function __get($var)
    {
        if (! array_key_exists($var, $this->fields)) {
            throw new \Exception("Unknown field '$var' for " . get_class($this));
        }
        return $this->fields[$var];
    }

    public function __call($method, $args)
    {
        $method = strtolower($method);
        if (array_key_exists($method, $this->fields)) {
            return $this->fields[$method];
        }
        if (
            preg_match("/get(.*)/i", $method, $matches) &&
             array_key_exists($matches[1], $this->fields)
        ) {
            return $this->fields[$matches[1]];
        }
        throw new \Exception("Unknown field '$method' for " . get_class($this));
    }

    public function requiredFields()
    {
        return array();
    }

    public static function entityFields()
    {
        return array();
    }

    public function getFields()
    {
        $ret = [];
        foreach ($this->entityFields() as $key) {
            $ret[$key] = isset($this->fields[$key]) ? $this->fields[$key] : null;
        }
        return $ret;
    }

    public function toArray()
    {
        return $this->getFields();
    }
}
