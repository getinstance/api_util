<?php

/*
 * This file is part of the getinstance/api_util framework.
 *
 * (c)2018 getInstance Ltd <matt@getinstance.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace getinstance\api_util\controller;

use DI\ContainerBuilder;
use Slim\App;

interface InitWareInterface
{
    public function handleConfiguration(ContainerBuilder $builder, Conf $conf): void;
    public function handleRouting(App $app): void;
}
