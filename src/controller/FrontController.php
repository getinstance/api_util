<?php

/*
 * This file is part of the getinstance/api_util framework.
 *
 * (c)2018 getInstance Ltd <matt@getinstance.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace getinstance\api_util\controller;

use DI\ContainerBuilder;
use Odan\Session\Middleware\SessionStartMiddleware;
use Odan\Session\PhpSession;
use Odan\Session\SessionInterface;
use Odan\Session\SessionManagerInterface;
use PDO;
use Psr\Container\ContainerInterface;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Log\LoggerInterface;
use Slim\Psr7\Factory\ResponseFactory;
use Slim\Factory\AppFactory;
use Slim\App;
use Slim\Middleware\ErrorMiddleware;
use Slim\Views\Twig;
use Slim\Views\TwigMiddleware;

use function DI\factory;

class FrontController
{
    private App $app;
    private ErrorMiddleware $errorMiddleware;

    /**
     * @var array<InitWareInterface>
     */
    private array $inits = [];

    public function __construct()
    {
    }

    public function getApp()
    {
        return $this->app;
    }

    public function addInitWare(InitWareInterface $init): void
    {
        $this->inits[] = $init;
    }

    public function formatException(Request $request, \Exception $e, ?Response $response = null)
    {
        error_log("exception: " . $e->getMessage());
        error_log("trace: " . $e->getTraceAsString());
        $data = [];

        $data['status'] = $e->getCode();
        $data['status'] = empty($data['status']) ? "500" : $data['status'];
        $data['msg'] = $e->getMessage();

        if ($data['status'] < 300 || $data['status'] > 599) {
            $data['msg'] .= " (also unknown error code '{$data['status']}')";
            $data['status'] = 500;
        }

        if (is_null($response)) {
            $responsefactory = new ResponseFactory();
            $response = $responsefactory->createResponse();
        }

        $payload = json_encode($data);
        $response->getBody()->write($payload);

        return $response
          ->withHeader('Content-Type', 'application/json')
          ->withStatus($data['status']);
    }

    public function init(array $params = [], Request $request = null)
    {
        $rootDir = (string)($params['rootDir'] ?? getcwd());
        $confpath = (isset($params['confpath'])) ? $params['confpath'] : null;
        $confname = (isset($params['confname'])) ? $params['confname'] : null;
        if (!is_null($confpath) && !is_null($confname)) {
            $conf = new Conf($confname, $confpath, Conf::RAISE_ERROR);
        } else {
            throw new \Exception(\sprintf(
                'invoke init with %s / %s providing at least a \'mode\' directive (dev/production)',
                '$params[\'confpath\']',
                '$params[\'confname\']',
            ));
        }

        // session settings
        $settings['session'] = [
            'name' => 'app',
            'lifetime' => 0,
            'path' => null,
            'domain' => null,
            'secure' => false,
            'httponly' => true,
            'cache_limiter' => 'nocache',
        ];

        //$this->app = new RouterApp();
        $containerBuilder = new ContainerBuilder();
        $containerBuilder->addDefinitions([
            Conf::class => factory(fn () => $conf),
            PDO::class => function () use ($conf) {
                $dbuser = $conf->get('dbuser');
                $dbname = $conf->get('dbname');
                $dbpass = $conf->get('dbpass');
                $dbhost = $conf->get('dbhost');
        
                $dsn = "mysql:dbname={$dbname};host={$dbhost};charset=utf8mb4";
                $pdo = new PDO($dsn, $dbuser, $dbpass);
                $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

                return $pdo;
            },
            "settings" => $settings,
            SessionManagerInterface::class => function (ContainerInterface $c) {
                return $c->get(SessionInterface::class);
            },
            SessionInterface::class => function (ContainerInterface $c) {
                $options = $c->get('settings')['session'];
                return new PhpSession($options);
            },
            App::class => function (ContainerInterface $c) {
                AppFactory::setContainer($c);
                return AppFactory::create();
            },
            LoggerInterface::class => function (ContainerInterface $c) {
                $path = $c->get(Conf::class)->get('error_log') ?? '';
                return new \getinstance\api_util\util\FileLogger($path, \Psr\Log\LogLevel::NOTICE);
            },
            Twig::class => function (ContainerInterface $c) use ($rootDir) {
                return Twig::create("$rootDir/views", ['cache' => false]);
            },
        ]);

        // allow addons and the implementing app to confingure the container
        foreach ($this->inits as $init) {
            $init->handleConfiguration($containerBuilder, $conf);
        }

        $container = $containerBuilder->build();

        $this->app = $container->get(App::class);
        $this->app->addRoutingMiddleware();

        // start session
        $this->app->add(SessionStartMiddleware::class);

        // https://akrabat.com/receiving-input-into-a-slim-4-application/
        $this->app->addBodyParsingMiddleware();

        /*
        // https://www.slimframework.com/docs/v4/objects/request.html
        $this->app->add(
            function (Request $request, RequestHandler $handler): Response {
                $contentType = $request->getHeaderLine('Content-Type');

                if (strstr($contentType, 'application/json')) {
                    $contents = json_decode((string)$request->getBody(), true);
                    if (json_last_error() === \JSON_ERROR_NONE) {
                        $request = $request->withParsedBody($contents);
                    }
                }
                return $handler->handle($request);
            }
        );
        */

        // Add Twig-View Middleware
        $this->app->add(TwigMiddleware::create($this->app, $container->get(Twig::class)));

        foreach ($this->inits as $init) {
            $init->handleRouting($this->app);
        }
        // hand back to implementing package
        // deprected use InitWare implementation
        $this->doRouting();

        // https://www.slimframework.com/docs/v4/middleware/error-handling.html
        $logger = $container->get(LoggerInterface::class);
        $this->errorMiddleware = $this->app->addErrorMiddleware(true, true, true, $logger);
    }

    // deprecated
    public function doRouting(): void
    {
    }

    public function execute()
    {
        return $this->app->run();
    }

    public function getErrorMiddleware(): ErrorMiddleware
    {
        return $this->errorMiddleware;
    }
}
