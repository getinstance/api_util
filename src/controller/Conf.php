<?php

/*
 * This file is part of the getinstance/api_util framework.
 *
 * (c)2018 getInstance Ltd <matt@getinstance.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace getinstance\api_util\controller;

class Conf
{
    private $vals = [];
    private $fullpath;
    public const RAISE_ERROR = 1;

    public function __construct($name, $path = "./", $attrs = 0)
    {
        $this->fullpath = \sprintf('%s/%s.ini', \rtrim($path, '/\\'), $name);
        if (file_exists($this->fullpath)) {
            $this->vals = parse_ini_file($this->fullpath);
        } elseif ((self::RAISE_ERROR & $attrs) > 0) {
            throw new \Exception("no conf file at '$this->fullpath'");
        }
    }

    public function get($key)
    {
        if (isset($this->vals[$key])) {
            return $this->vals[$key];
        }
        return null;
    }
}
