<?php

/*
 * This file is part of the getinstance/api_util framework.
 *
 * (c)2018 getInstance Ltd <matt@getinstance.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace getinstance\api_util\clicommand;

use Aura\Cli\Context;
use Aura\Cli\Stdio;

class DefaultCmd
{
    public function execute(Context $context, Stdio $stdio): void
    {
        $stdio->outln("hello");
    }
}
