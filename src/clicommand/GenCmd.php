<?php

/*
 * This file is part of the getinstance/api_util framework.
 *
 * (c)2018 getInstance Ltd <matt@getinstance.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */


namespace getinstance\api_util\clicommand;

use Twig\Loader\FilesystemLoader;
use Twig\Environment;

abstract class GenCmd
{
    private $twig;
    private $clobber;

    public function __construct()
    {
        $this->getTwig();
    }


    public function genCore(string $appname, string $pkg): void
    {
        if (preg_match("/[^a-zA-Z0-9]/", $appname)) {
            throw new \Exception("the appname must be strictly alphanumeric and without spaces");
        }
        $args =  [
            "appname" => $appname,
            "package" => $pkg
        ];
        $this->genTemplate("index.tpl", "web/", "index.php", $args);
        $this->genTemplate("dockerfile.tpl", "dockerbuild", "Dockerfile", $args);
        $this->genTemplate("dockercompose.tpl", ".", "docker-compose.yaml", $args);
        $this->genTemplate("php.ini.tpl", "conf/", "php.ini", $args);
    }

    protected function getTwig(): void
    {
        $this->clobber = $this->getTmpDir();

        $paths = __DIR__ . "/../apputil/res/gentemplates";

        $loader = new FilesystemLoader($paths);
        $twig = new Environment($loader, array(
        //    'cache' => '/tmp/twigcache',
        ));
        $this->twig = $twig;
    }

    protected function getTmpDir(): string
    {
        $base = "tmp/clobber";
        for ($x = 0; $x < 200; $x++) {
            $suffix = str_pad("{$x}", 3, "0");
            $dir = "{$base}.{$suffix}";
            if (! file_exists($dir)) {
                mkdir($dir, 0755, true);
                return $dir;
            }
        }
    }

    public function outputTemplate($template, $dirpath, $targetfile, $args): void
    {
        print "\n";
        print "## $template : ########################################## \n\n";
        $regtext = $this->twig->render($template, $args);
        print $regtext;
        print "\n";
    }

    public function genTemplate(array|string $template, $dirpath, $targetfile, $args): void
    {
        if (is_string($template)) {
            $template = [$template];
        }
        $origpath = $dirpath;
        $dirpath = $this->getOutputPath($dirpath, $targetfile);

        $regpath = "{$dirpath}/{$targetfile}";
        print "\n";
        print "## " . implode(",", $template) . " : ########################################## \n\n";
        if ($origpath != $dirpath) {
            print "# CLOBBER - creating tmpfile at '{$regpath}'\n";
        }
        if (! file_exists($dirpath)) {
            mkdir($dirpath, 0755, true);
        }

        if (file_exists($regpath)) {
            // something is up.. clobber file exists
            throw new \Exception("{$regpath} exists");
        } else {
            $regtext = "";
            foreach ($template as $t) {
                $regtext .= $this->twig->render($t, $args);
            }
            file_put_contents($regpath, $regtext);
        }
        print "\n";
    }

    protected function getOutputPath($dirpath, $targetfile): string
    {
        $fullpath = "{$dirpath}/{$targetfile}";
        if (file_exists($fullpath)) {
            $dirpath = $this->clobber . "/$dirpath";
        }
        return $dirpath;
    }

    protected function createEmptyFile($dirpath, $targetfile): void
    {
        if (! file_exists($dirpath)) {
            mkdir($dirpath, 0755, true);
        }
        $regpath = "{$dirpath}/{$targetfile}";
        if (! file_exists($regpath)) {
            file_put_contents($regpath, "");
        }
    }
}
