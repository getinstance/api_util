<?php

/*
 * This file is part of the getinstance/api_util framework.
 *
 * (c)2018 getInstance Ltd <matt@getinstance.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */


namespace getinstance\api_util\clicommand;

use Aura\Cli\Context;
use Aura\Cli\Stdio;

class GenWebCmd extends GenCmd
{
    public function execute(Context $context, Stdio $stdio)
    {
        $appname = $context->argv->get(2);
        $pkg = $context->argv->get(3);
        $args =  [
            "appname" => $appname,
            "package" => $pkg,
            // sample values for generating proper SQL scripts
            'endpoint' => 'modelObjectA',
            'fields' => ['field1', 'field2', 'field3'],
        ];

        $this->genCore($appname, $pkg);

        // view
        $this->genTemplate("webview.tpl", "views/", "main.twig", $args);
        $this->genTemplate("webcommand.tpl", "src/command", "Main.php", $args);
        $this->genTemplate("webcontroller.tpl", "src/controller", "Controller.php", $args);

        $this->createEmptyFile("conf", "{$appname}.ini");

        // we'll still create a db in web mode for dev
        $this->genTemplate('createdbsql.tpl', 'scripts', 'create-db.sql', $args);
        $this->genTemplate('sql.tpl', 'scripts', 'create-schema.sql', $args);
    }
}
