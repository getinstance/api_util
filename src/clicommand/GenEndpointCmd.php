<?php

/*
 * This file is part of the getinstance/api_util framework.
 *
 * (c)2018 getInstance Ltd <matt@getinstance.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */


namespace getinstance\api_util\clicommand;

use Aura\Cli\Context;
use Aura\Cli\Stdio;

class GenEndpointCmd extends GenCmd
{
    public function execute(Context $context, Stdio $stdio)
    {
        $appname = $context->argv->get(2);
        $pkg = $context->argv->get(3);

        $endpoint = $context->argv->get(4);
        $fieldstring = $context->argv->get(5);
        $arglist = explode(",", $fieldstring);

        $object = ucfirst($endpoint);
        $args =  [
            "appname" => $appname,
            "object" => $object,
            "endpoint" => $endpoint,
            "fields" => $arglist,
            "package" => $pkg
        ];
        $testargs = $args;
        $testargs['appname'] = "{$args['appname']}_test";

        $this->genCore($appname, $pkg);

        // core db
        $this->genTemplate("createdb.tpl", "scripts/", "createvagrantdb", $args);
        $this->genTemplate("createdb.tpl", "scripts/", "createvagranttestdb", $testargs);
        $this->genTemplate("createdbsql.tpl", "scripts/", "create-db.sql", $args);
        $this->genTemplate("resetdb.tpl", "scripts/", "resetvagrantdb", $args);
        $this->genTemplate("resetdb.tpl", "scripts/", "resetvagranttestdb", $testargs);

        // core everything
        $this->genTemplate("conf.tpl", "conf/", "{$appname}.ini", $args);
        $this->genTemplate("conf.tpl", "conf/", "{$appname}_test.ini", $testargs);
        $this->createEmptyFile("views", ".placeholder");

        // endpoint model
        $this->genTemplate("model.tpl", "src/model", "{$object}.php", $args);

        // endpoint commands
        $this->genTemplate("getcommand.tpl", "src/command", "Get{$object}.php", $args);
        $this->genTemplate("getcollectioncommand.tpl", "src/command", "Get{$object}Collection.php", $args);
        $this->genTemplate("postcommand.tpl", "src/command", "Post{$object}.php", $args);
        $this->genTemplate("putcommand.tpl", "src/command", "Put{$object}.php", $args);
        $this->genTemplate("deletecommand.tpl", "src/command", "Delete{$object}.php", $args);

        // endpoint controller
        $this->genTemplate("controller.tpl", "src/controller", "Controller.php", $args);

        // endpoint db
        $this->genTemplate("mapper.tpl", "src/persist", "{$object}Mapper.php", $args);
        $this->genTemplate("sql.tpl", "scripts", "create-schema.sql", $args);

        // endpoint test
        $this->createEmptyFile('scripts/test', 'basedata.sql');
        $this->genTemplate('createdbsql.tpl', 'scripts/test', 'create-db.sql', $testargs);
        $this->genTemplate('sql.tpl', 'scripts/test', 'create-schema.sql', $testargs);
        $this->genTemplate('testbootstrap.tpl', 'test/res/', 'bootstrap.php', $testargs);
        $this->genTemplate('test.tpl', 'test/functional/controller/', 'ControllerTest.php', $testargs);
        $this->genTemplate('basetest.tpl', 'test/functional/testutil/', 'Base.php', $testargs);
    }
}
