<?php

/*
 * This file is part of the getinstance/api_util framework.
 *
 * (c)2018 getInstance Ltd <matt@getinstance.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace getinstance\api_util\service;

use GuzzleHttp\Client;
use Psr\Http\Message\ResponseInterface;

/**
 * The web client that makes requests to a REST API and expects JSON responses.
 */
class RestClient
{
    private string $baseUri;
    private Client $client;
    private array $headers;

    /**
     * Constructor.
     *
     * @param string                $baseUri The base URI of the REST API. Each request will be relative to this URI.
     * @param array<string, string> $headers The headers to send with each request.
     */
    public function __construct(string $baseUri, array $headers = [])
    {
        $this->baseUri = $baseUri;
        if (!isset($headers['Accept'])) {
            $headers['Accept'] = 'application/json';
        }
        $this->headers = $headers;
        $this->client = new Client();
    }

    /**
     * Sends a PUT request.
     *
     * @param string       $path Relative path to the resource.
     * @param array<mixed> $json The JSON payload.
     *
     * @return array<mixed> The response status and the decoded JSON body.
     *
     * @psalm-return array{0: int, 1: array<mixed>}
     */
    protected function doPut(string $path, array $json = []): array
    {
        $args = $this->wrapArgs([
            'json' => $json,
        ]);
        $response = $this->client->put($this->baseUri . $path, $args);
        return $this->parseResponse($response);
    }

    /**
     * Sends a POST request.
     *
     * @param string       $path Relative path to the resource.
     * @param array<mixed> $json The JSON payload.
     *
     * @return array<mixed> The response status and the decoded JSON body.
     *
     * @psalm-return array{0: int, 1: array<mixed>}
     */
    protected function doPost(string $path, array $json = []): array
    {
        $args = $this->wrapArgs([
            'json' => $json,
        ]);
        $response = $this->client->post($this->baseUri . $path, $args);
        return $this->parseResponse($response);
    }

    /**
     * Sends a DELETE request.
     *
     * @param string                $path  Relative path to the resource.
     * @param array<string, string> $query The query string parameters.
     *
     * @return array<mixed> The response status and the decoded JSON body.
     *
     * @psalm-return array{0: int, 1: array<mixed>}
     */
    protected function doDelete(string $path, array $query = []): array
    {
        $args = $this->wrapArgs([
            'query' => $query,
        ]);
        $response = $this->client->delete($this->baseUri . $path, $args);
        return $this->parseResponse($response);
    }

    /**
     * Sends a GET request.
     *
     * @param string                $path  Relative path to the resource.
     * @param array<string, string> $query The query string parameters.
     *
     * @return array<mixed> The response status and the decoded JSON body.
     *
     * @psalm-return array{0: int, 1: array<mixed>}
     */
    protected function doGet(string $path, array $query = []): array
    {
        $args = $this->wrapArgs([
            'query' => $query,
        ]);
        $response = $this->client->get($this->baseUri . $path, $args);
        return $this->parseResponse($response);
    }

    /**
     * Parses the response into a status code and a decoded JSON body.
     *
     * @param ResponseInterface $response The response to parse.
     *
     * @return array<mixed> The response status and the decoded JSON body.
     *
     * @psalm-return array{0: int, 1: array<mixed>}
     */
    private function parseResponse(ResponseInterface $response): array
    {
        $json = $response->getBody()->getContents();
        $body = json_decode($json, true);
        $code = $response->getStatusCode();
        return [$code, $body];
    }

    /**
     * Injects request options with the headers and http_errors options.
     *
     * @param array<string, mixed> $orig The original request options.
     *
     * @return array<string, mixed> The updated request options.
     */
    private function wrapArgs(array $orig): array
    {
        $orig['headers'] = $this->headers;
        $orig['http_errors'] = false;
        return $orig;
    }
}
