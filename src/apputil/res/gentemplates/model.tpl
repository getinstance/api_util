<?php

namespace {{ package }}\model;

use getinstance\commons\model\ModelRecord;
use getinstance\commons\model\DateType;

class {{ object }} extends ModelRecord
{
    public function requiredFields(): array
    {
        return [
            "id",
    {% for field in fields %}
        "{{ field }}",
    {% endfor %}
        ];
    }

    public static function entityFields(): array
    {
        return [
            "id",
        {% for field in fields %}
            "{{ field }}",
        {% endfor %}
        ];
    }
}
