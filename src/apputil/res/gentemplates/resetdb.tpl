echo 'DROP DATABASE IF EXISTS {{ appname }}' | mysql -u vagrant -pvagrant {{ appname }}
echo 'CREATE DATABASE {{ appname }}' | mysql -u vagrant -pvagrant
cat scripts/create-db.sql | mysql -u vagrant -pvagrant {{ appname }}
cat scripts/create-schema.sql | mysql -u vagrant -pvagrant {{ appname }}
