version: '3.7'

services:
    {{ appname }}:
        #image: php:8-alpine
        build: ./dockerbuild
        working_dir: /var/www
        command: php -S 0.0.0.0:8080 -c conf/php.ini -t web 
        ports:
            - "8080:8080"
        volumes:
            - .:/var/www
        networks:
          util: 
            aliases:
              - webui

    db:
        image: mariadb
        restart: always
        environment:
          MARIADB_ROOT_PASSWORD: vagrant
        volumes:
          - ./scripts/create-db.sql:/docker-entrypoint-initdb.d/01.sql
          - ./scripts/test/create-db.sql:/docker-entrypoint-initdb.d/02.sql
          - ./scripts/create-schema.sql:/docker-entrypoint-initdb.d/03.sql
          - ./scripts/test/create-schema.sql:/docker-entrypoint-initdb.d/04.sql
        ports:
            - "3306:3306"
        networks:
          util: 
            aliases:
              - db
networks:
  util: 
