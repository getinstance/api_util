CREATE DATABASE IF NOT EXISTS `{{ appname }}`;
CREATE USER IF NOT EXISTS 'vagrant'@'%';
GRANT ALL PRIVILEGES ON {{ appname }}.* TO 'vagrant'@'%' IDENTIFIED BY 'vagrant' WITH GRANT OPTION;
FLUSH PRIVILEGES;
use {{ appname }};

