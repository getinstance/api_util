USE {{ appname }};

CREATE TABLE IF NOT EXISTS `{{ endpoint }}` (
    `id` int(11) NOT NULL AUTO_INCREMENT,
    {% for field in fields %}
    `{{ field }}` VARCHAR(256) NOT NULL,
    {% endfor %}
    PRIMARY KEY (`id`)
);
