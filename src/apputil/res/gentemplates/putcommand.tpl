<?php

namespace {{ package }}\command;

use getinstance\api_util\command\Command;
use {{ package }}\model\{{ object }};
use {{ package }}\persist\{{ object }}Mapper;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;

class Put{{ object }} extends Command
{
    private {{ object }}Mapper ${{ endpoint }}mapper;

    public function __construct({{ object }}Mapper ${{ endpoint }}mapper)
    {
        $this->{{ endpoint }}mapper = ${{ endpoint }}mapper;
    }

    public function execute(Request $request, Response $response, array $args): Response
    {
        $id = $args['id'];
        $all = $this->allRequestParams($request);
        $all['id'] = $id;

        // fill this out
        $required = [];
        $row = $this->doRequired($request, $required);

        $obj = new {{ object }}($all);
        $this->{{ endpoint }}mapper->save{{ object }}($obj);

        return $this->jsonResponse($request, [
            [
            "success" => true,
            "msg" => "saved",
            "data" => $obj->toScalarArray()
            ],
            200
        ]);
    }
}
