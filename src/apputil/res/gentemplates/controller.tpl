<?php

namespace {{ package }}\controller;

use getinstance\api_util\controller\InitWareInterface;
use getinstance\api_util\controller\Conf;
use DI\ContainerBuilder;
use Psr\Container\ContainerInterface;
use Slim\App;

class Controller implements InitWareInterface 
{
    public function handleConfiguration(ContainerBuilder $builder, Conf $conf): void
    {

    }

    public function handleRouting(App $app): void
    {
        $app->post('/{{ endpoint }}', '{{ package }}\command\Post{{ object }}:execute');
        $app->put('/{{ endpoint }}/{id}', '{{ package }}\command\Put{{ object }}:execute');
        $app->get('/{{ endpoint }}/{id}', '{{ package }}\command\Get{{ object }}:execute');
        $app->get('/{{ endpoint }}s', '{{ package }}\command\Get{{ object }}Collection:execute');
        $app->delete('/{{ endpoint }}/{id}', '{{ package }}\command\Delete{{ object }}:execute');
    }
}
