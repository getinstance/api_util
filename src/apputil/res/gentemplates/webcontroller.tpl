<?php

namespace {{ package }}\controller;

use getinstance\api_util\controller\InitWareInterface;
use getinstance\api_util\controller\Conf;
use DI\ContainerBuilder;
use Slim\App;

class Controller implements InitWareInterface 
{
    public function handleConfiguration(ContainerBuilder $builder, Conf $conf): void
    {

    }

    public function handleRouting(App $app): void
    {
        $app->get('/', '{{ package }}\command\Main:main');
    }
}
