<?php

namespace {{ package }}\persist;

use getinstance\api_util\persist\Mapper;
use {{ package }}\model\{{ object }};

class {{ object }}Mapper extends Mapper
{
    private $pdo;

    public function __construct(\PDO $pdo)
    {
        $this->pdo = $pdo;
    }

    public function getValidFilterFields(): array
    {
        return {{ object }}::entityFields();
    }

    public function get{{ object }}(int|string $id): {{ object }}
    {
        $sql = "SELECT * FROM {{ endpoint }} where id=?";
        $stmt = $this->pdo->prepare($sql);
        $stmt->execute([$id]);
        $row = $stmt->fetch();
        if (! is_array($row)) {
            throw new \Exception("{{ endpoint }} '$id' not found");
        }
        $ret = {{ object }}::fromScalarArray($row);
        return $ret;
    }

    public function get{{ object }}s(array $filter = [], array $order = [], string $filtermode="AND"): array
    {
        $filtermode = ($filtermode == "OR")?"OR":"AND"; 
        $legalfields = {{ object }}::entityFields();
        $sql = "SELECT * FROM {{ endpoint }}";
        list($where, $vals) = $this->manageFilters($filter);
        if (! empty($where)) {
            $sql .= " WHERE " . implode(" {$filtermode} ", $where);
        }

        $order = $this->getOrderString($order, $legalfields);
        if (! empty($order)) {
            $sql .= " ORDER BY {$order}";
        }

        $stmt = $this->pdo->prepare($sql);
        $stmt->execute($vals);
        $ret = [];
        while ($row = $stmt->fetch()) {
            $ret[] = {{ object }}::fromScalarArray($row);
        }
        return $ret;
    }

    public function save{{ object }}({{ object }} ${{ endpoint }}): bool
    {
        if (${{ endpoint }}->getField("id") < 0) {
            return $this->insert{{ object }}(${{ endpoint }});
        } else {
            return $this->update{{ object }}(${{ endpoint }});
        }
    }

    public function insert{{ object }}({{ object }} ${{ endpoint }}): bool
    {
        $fields = ${{ endpoint }}->toScalarArray();
        $legalfields = {{ object }}::entityFields();

        list($settings, $vals) = $this->getSettingString($fields, $legalfields);
        $sql = "INSERT INTO {{ endpoint }} SET {$settings}";
        $stmt = $this->pdo->prepare($sql);
        $stmt->execute($vals);
        ${{ endpoint }}->setField("id", $this->pdo->lastinsertid());
        return true;
    }

    public function update{{ object }}({{ object }} ${{ endpoint }}): bool
    {

        $fields = ${{ endpoint }}->toScalarArray();
        $legalfields = {{ object }}::entityFields();

        list($settings, $vals) = $this->getSettingString($fields, $legalfields);
        $sql = "UPDATE {{ endpoint }} SET {$settings} WHERE id=?";
        $vals[] = ${{ endpoint }}->getField("id");

        $stmt = $this->pdo->prepare($sql);
        $stmt->execute($vals);
        return true;
    }

    public function delete{{ object }}({{ object }} ${{ endpoint }}): bool
    {

        $fields = ${{ endpoint }}->toScalarArray();
        $legalfields = {{ object }}::entityFields();

        //list($settings, $vals) = $this->getSettingString($fields, $legalfields);
        $vals = [];
        $sql = "DELETE FROM {{ endpoint }} WHERE id=?";
        $vals[] = ${{ endpoint }}->getField("id");

        $stmt = $this->pdo->prepare($sql);
        $stmt->execute($vals);
        return true;
    }
}
