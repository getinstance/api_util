<?php

namespace {{ package }}\controller;

use {{ package }}\testutil\Base;

class ControllerTest extends Base
{

    /**
     * @runInSeparateProcess
     * https://stackoverflow.com/questions/9745080/test-php-headers-with-phpunit
     */
    public function test{{ object }}(): void
    {
        $fieldname='{{ fields[0] }}';
        $fieldval='111';

        list($code, $contents, $resp) = $this->post("/{{ endpoint }}", [
            {% for fieldname in fields %}
            '{{ fieldname }}' => '111',
            {% endfor %}
            ]
        );
        self::assertEquals($code, 201);
        self::assertEquals($contents['data'][$fieldname], $fieldval);

        list($code, $contents, $resp) = $this->get("/{{ endpoint }}/{$contents['data']['id']}");
        self::assertEquals($code, 200);
        self::assertEquals($contents['data'][$fieldname], $fieldval);


        list($code, $contents, $resp) = $this->get("/{{ endpoint }}s", ["filter" => "id={$contents['data']['id']}"]);
        self::assertEquals($code, 200);
        self::assertEquals($contents['data']['rows'][0][$fieldname], $fieldval);
    }
}
