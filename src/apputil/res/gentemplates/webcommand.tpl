<?php

namespace {{ package }}\command;

use getinstance\api_util\command\Command;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;

class Main extends Command
{
    public function main(Request $request, Response $response): Response
    {
        $vals = [];
        return $this->twigResponse($request, "main.twig", $vals, $response);
    }
}
