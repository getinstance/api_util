<?php
$ini = __DIR__."/../../conf/{{ appname }}.ini";
$scriptdir = __DIR__."/../../scripts";

$conf = parse_ini_file($ini);

catsql("{$scriptdir}/test/basedata.sql", $conf);

function runsql(string $sql, array $conf): void
{
    $cmd =  "echo \"{$sql}\" ";
    $cmd .= "| mysql -uroot -p{$conf['rootpass']} ";
    $cmd .= " -h{$conf['dbhost']} ";
    #echo $cmd . "\n";
    `$cmd`;
}

function catsql(string $sqlfile, array $conf): void
{
    $cmd =  "cat {$sqlfile} ";
    $cmd .= "| mysql -uroot -p{$conf['rootpass']} ";
    $cmd .= " -h{$conf['dbhost']} {$conf['dbname']}";
    #echo $cmd . "\n";
    `$cmd`;
}
