<?php

namespace {{ package }}\command;

use getinstance\api_util\command\Command;
use {{ package }}\model\{{ object }};
use {{ package }}\persist\{{ object }}Mapper;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;

class Post{{ object }} extends Command
{
    private {{ object }}Mapper ${{ endpoint }}mapper;

    public function __construct({{ object }}Mapper ${{ endpoint }}mapper)
    {
        $this->{{ endpoint }}mapper = ${{ endpoint }}mapper;
    }

    public function execute(Request $request, Response $response): Response
    {
        $all = $this->allRequestParams($request);
        $all["id"] = -1;

        // fill this out
        $required = [];
        $row = $this->doRequired($request, $required);

        $obj = new {{ object }}($all);
        $this->{{ endpoint }}mapper->save{{ object }}($obj);

        return $this->jsonResponse($request, [
            [
            "success" => true,
            "msg" => "created",
            "data" => $obj->toScalarArray()
            ],
            201
        ]);
    }
}
