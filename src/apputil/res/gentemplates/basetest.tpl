<?php

namespace {{ package }}\testutil;

use getinstance\api_util\util\WebTestCase;
use getinstance\api_util\controller\InitWareInterface;

use {{ package }}\controller\Controller;


class Base extends WebTestCase
{
    public function setUp(): void
    {
        parent::setUp();
        $this->doSetUp();
    }

    public function doSetUp(): void
    {
        // do nothing
    }

    public function getConfPath(): string
    {
        return __DIR__."/../../../conf/";
    }

    public function getConfName(): string
    {
        return '{{ appname }}';
    }

    public function getInitWare(): InitWareInterface 
    {
        return new Controller();
    }
}
