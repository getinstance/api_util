<?php

namespace {{ package }}\command;

use getinstance\api_util\command\Command;
use {{ package }}\model\{{ object }};
use {{ package }}\persist\{{ object }}Mapper;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;

class Get{{ object }} extends Command
{
    private {{ object }}Mapper ${{ endpoint }}mapper;

    public function __construct({{ object }}Mapper ${{ endpoint }}mapper)
    {
        $this->{{ endpoint }}mapper = ${{ endpoint }}mapper;
    }


    public function execute(Request $request, Response $response, array $args): Response
    {
        $id = $args['id'];
        $obj = $this->{{ endpoint }}mapper->get{{ object }}($id);


        return $this->jsonResponse($request, [
            [
            "success" => true,
            "msg" => "found",
            "data" => $obj->toScalarArray()
            ],
            200
        ]);
    }
}
