<?php

namespace {{ package }}\command;

use getinstance\api_util\command\Command;
use {{ package }}\model\{{ object }};
use {{ package }}\persist\{{ object }}Mapper;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;

class Get{{ object }}Collection extends Command
{
    private {{ object }}Mapper ${{ endpoint }}mapper;

    public function __construct({{ object }}Mapper ${{ endpoint }}mapper)
    {
        $this->{{ endpoint }}mapper = ${{ endpoint }}mapper;
    }

    public function execute(Request $request, Response $response): Response
    {
        $queryvals = $request->getQueryParams();
        $filtermode = $queryvals['filtermode'] ?? "";
        $filtermode = (strtolower($filtermode) == "or") ? "OR" : "AND";
        $filters = $this->getFilterArray($request);
        $order = $this->doOrder($request);

        $items = [];
        try {
            ${{ endpoint }}s = $this->{{ endpoint }}mapper->get{{ object }}s($filters, $order, $filtermode);
            foreach (${{ endpoint }}s as ${{ endpoint }}) {
                $items[] = ${{ endpoint }}->toScalarArray();
            }
        } catch (\Exception $e) {
            return $this->jsonResponse($request, [["msg" => $e->getMessage()], 500]);
        }

        return $this->jsonResponse($request, [
            [
            "success" => true,
            "msg" => "acquired",
            "data" => [
                "rows" => $items
                ]
            ],
            200
        ]);
    }
}
