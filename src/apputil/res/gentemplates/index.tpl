<?php
require_once __DIR__.'/../vendor/autoload.php';

use getinstance\api_util\controller\FrontController;
use {{ package }}\controller\Controller;

$controller = new FrontController();
$appController = new Controller();

$controller->addInitWare($appController);

$rootDir = dirname(__DIR__);
$controller->init([
    'rootDir' => $rootDir,
    'confpath' => "$rootDir/conf/",
    'confname'=>'{{ appname }}',
]);

$response = $controller->execute();
