<?php

/*
 * This file is part of the getinstance/api_util framework.
 *
 * (c)2018 getInstance Ltd <matt@getinstance.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace getinstance\api_util\mail;

use Twig\Loader\FilesystemLoader;
use Twig\Environment;
use getinstance\commons\model\ModelRecord;
use getinstance\commons\model\SerialisableArrayType;

class MailBase extends ModelRecord
{
    public function __construct(array $args)
    {
        if (! isset($args['id'])) {
            $args['id'] = -1;
        }
        $args['type'] = $this->mailType();
        parent::__construct($args);
        $this->enforceMailFields();
    }

    public function mailType()
    {
        return "MailBase";
    }

    public function requiredFields()
    {
        return [
            "id",
            "type",
            "tomail",
            "frommail",
            "subject",
            "mailfields",
        ];
    }

    public function enforceMailFields()
    {
        $fields = $this->getField("mailfields");
        $reqfields = $this->requiredMailFields();
        foreach ($reqfields as $req) {
            if (! isset($fields[$req])) {
                throw new \Exception("mail field '$req' is required for type " . $this->getField("type"));
            }
        }
    }

    public function requiredMailFields()
    {
        return array();
    }

    public static function enforcedTypes()
    {
        return array(
            "mailfields" => new SerialisableArrayType()
        );
    }

    public function toArray()
    {
        $base = parent::toArray();
        return $base;
    }

    public function generatePlainMessage()
    {
        return $this->doGenerateMessage("plain");
    }

    public function generateMessage()
    {
        return $this->doGenerateMessage();
    }

    private function doGenerateMessage($suffix = "")
    {
        if (! empty($suffix)) {
            $suffix = ".{$suffix}";
        }

        $type = $this->getField("type");
        $templatename = "{$type}{$suffix}.twig";
        $fields = $this->getField("mailfields");

        // TODO - support a measure of recursion - ie %fields% that themselves have %fields%
        foreach ($fields as $outerkey => $outerval) {
            foreach ($fields as $innerkey => $innerval) {
                $fields[$innerkey] = preg_replace("/%{$outerkey}%/", $outerval, $innerval);
            }
        }

        return $this->doTwig($templatename, $fields);
    }

    public function doTwig($name, array $vals = [])
    {
        $mailDir = $this->getTemplateDir();
        $loader = new FilesystemLoader($mailDir);
        $twig = new Environment($loader, []);
        return $twig->render($name, $vals);
    }

    protected function getTemplateDir(): string
    {
        $dir = $this->getField('templatedir', '');
        if ($dir === '') {
            $dir = __DIR__;
            while ($dir !== '/' && !\file_exists("$dir/mailtemplates")) {
                $dir = \dirname($dir);
            }
            $dir .= '/mailtemplates';
        }

        return $dir;
    }

    public static function entityFields()
    {
        return [
            "id",
            "subject",
            "tomail",
            "toname",
            "frommail",
            "fromname",
            "type",
            "mailfields",
            'templatedir',
        ];
    }
}
