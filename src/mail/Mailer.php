<?php

/*
 * This file is part of the getinstance/api_util framework.
 *
 * (c)2018 getInstance Ltd <matt@getinstance.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */


namespace getinstance\api_util\mail;

abstract class Mailer
{
    public function getMessage(MailBase $mail)
    {
        return $mail->generateMessage();
    }

    public function getPlainMessage(MailBase $mail)
    {
        return $mail->generatePlainMessage();
    }

    abstract public function sendMail(MailBase $mail);
}
