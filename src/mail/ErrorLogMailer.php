<?php

/*
 * This file is part of the getinstance/api_util framework.
 *
 * (c)2018 getInstance Ltd <matt@getinstance.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */


namespace getinstance\api_util\mail;

class ErrorLogMailer extends Mailer
{
    public function sendMail(MailBase $mail)
    {
        $to = $mail->getField("tomail");
        $msg = $this->getMessage($mail);
        error_log("to: $to\n\n$msg\n");
    }
}
