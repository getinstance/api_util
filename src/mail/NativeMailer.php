<?php

/*
 * This file is part of the getinstance/api_util framework.
 *
 * (c)2018 getInstance Ltd <matt@getinstance.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace getinstance\api_util\mail;

use PHPMailer\PHPMailer\PHPMailer;

class NativeMailer extends Mailer
{
    public function sendMail(MailBase $mail)
    {
        $to = $mail->getField("tomail");

        $subject = $mail->getField("subject");

        $msg = $this->getMessage($mail);
        $plainmsg = $this->getPlainMessage($mail);
        $frommail = $mail->getField("frommail");

        $mail = new PHPMailer(true);
        $mail->isMail();
        $mail->setFrom($frommail, 'Socializer');
        $mail->addAddress($to);     // Add a recipient

        error_log("to: $to / from: $frommail / subject: $subject");
        error_log($msg);
        error_log($plainmsg);

        //Content
        $mail->isHTML(true);                                  // Set email format to HTML
        $mail->Subject = $subject;
        $mail->Body    = $msg;
        $mail->AltBody = $plainmsg;
        $mail->send();
    }
}
