<?php

namespace getinstance\api_util\mail;

use PHPMailer\PHPMailer\PHPMailer;

class MailtrapMailer extends Mailer
{
    private readonly string $username;
    private readonly string $password;

    public function __construct(string $username, string $password)
    {
        $this->username = $username;
        $this->password = $password;
    }

    #region extends Mailer

    public function sendMail(MailBase $mail)
    {
        $phpmailer = new PHPMailer();
        $phpmailer->isSMTP();
        $phpmailer->Host = 'sandbox.smtp.mailtrap.io';
        $phpmailer->SMTPAuth = true;
        $phpmailer->Port = 2525;
        $phpmailer->Username = $this->username;
        $phpmailer->Password = $this->password;

        $phpmailer->setFrom($mail->getField('frommail'));
        $phpmailer->addAddress($mail->getField('tomail'));
        $phpmailer->Subject = $mail->getField('subject');
        $phpmailer->Body = $this->getMessage($mail);
        $phpmailer->AltBody = $this->getPlainMessage($mail);
        $phpmailer->send();
    }

    #endregion extends Mailer
}
