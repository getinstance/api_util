<?php

declare(strict_types=1);

namespace getinstance\api_util\mail;

use GuzzleHttp\Client;

/**
 * Represents a mailing service that uses the Mailgun API.
 */
class MailgunMailer extends Mailer
{
    private string $apiKey;
    private string $baseUrl;

    /**
     * Creates a new MailgunMailer instance.
     *
     * @param string $apiKey        The Mailgun API key.
     * @param string $region        Mailgun region (either 'eu' or 'us').
     * @param string $sendingDomain The sending domain.
     */
    public function __construct(string $apiKey, string $region, string $sendingDomain)
    {
        $this->apiKey = $apiKey;
        $this->baseUrl = match ($region) {
            'eu' => 'https://api.eu.mailgun.net',
            default => 'https://api.mailgun.net',
        };
        $this->baseUrl .= "/v3/$sendingDomain";
    }

    /**
     * @inheritDoc
     */
    public function sendMail(MailBase $mail)
    {
        $client = new Client();
        $url = $this->baseUrl . '/messages';

        $form = [
            'type' => 'generic',
            'from' => $mail->getField('frommail'),
            'to' => $mail->getField('tomail'),
            'subject' => $mail->getField('subject'),
        ];
        $msg = $this->getMessage($mail);
        $plainMsg = $this->getPlainMessage($mail);
        if ($msg !== '') {
            $form['html'] = $msg;
        }
        if ($plainMsg !== '') {
            $form['text'] = $plainMsg;
        }

        $params = [
            'auth' => ['api', $this->apiKey],
            'form_params' => $form,
        ];
        $client->request('POST', $url, $params);
    }
}
