<?php

/*
 * This file is part of the getinstance/api_util framework.
 *
 * (c)2018 getInstance Ltd <matt@getinstance.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */


namespace getinstance\api_util\persist;

use getinstance\api_util\mail\MailBase;
use getinstance\api_util\persist\Mapper;

class MockMailMapper extends Mapper
{
    private $pdo;

    public function __construct(\PDO $pdo)
    {
        $this->pdo = $pdo;
    }

    public function getValidFilterFields()
    {
        return MockMail::entityFields();
    }

    public function getMockMail($id)
    {
        // remember these are mocks - there is no object to represent them
        $sql = "SELECT * FROM mockmail where id=?";
        $stmt = $this->pdo->prepare($sql);
        $stmt->execute([$id]);
        $row = $stmt->fetch();
        if (! is_array($row)) {
            throw new \Exception("mockmail '$id' not found");
        }
        $ret = $row;
        return $ret;
    }

    public function getMockMails($filter = [], $limit = 25)
    {
        // remember these are mocks - there is no object to represent them
        $sql = "SELECT * FROM mockmail";
        list($where, $vals) = $this->manageFilters($filter);
        if (! empty($where)) {
            $sql .= " WHERE " . implode(" AND ", $where);
        }
        $sql .= " ORDER BY id desc";
        $sql .= " LIMIT {$limit}";
        $stmt = $this->pdo->prepare($sql);
        $stmt->execute($vals);
        $ret = [];
        while ($row = $stmt->fetch()) {
            $ret[] = $row;
        }
        return $ret;
    }

    public function saveMockMail(MailBase $mockmail)
    {
        if ($mockmail->id() < 0) {
            return $this->insertMockMail($mockmail);
        } else {
            return $this->updateMockMail($mockmail);
        }
    }

    public function makeFieldsAndLegal(MailBase $mockmail)
    {
        $fields = $mockmail->toScalarArray();

        // we are saving a subset of the object
        // $legalfields = MockMail::entityFields();

        $fields['message'] = $mockmail->generateMessage();
        $fields['date'] = (new \DateTime("now", new \DateTimeZone("UTC")))->format("c");
        $legalfields = [
            "tomail",
            "toname",
            "frommail",
            "fromname",
            "subject",
            "date",
            "message"
        ];
        return [$fields, $legalfields];
    }

    public function insertMockMail(MailBase $mockmail)
    {
        list($fields, $legalfields) = $this->makeFieldsAndLegal($mockmail);
        list($settings, $vals) = $this->getSettingString($fields, $legalfields);
        $sql = "INSERT INTO mockmail SET {$settings}";
        $stmt = $this->pdo->prepare($sql);
        $stmt->execute($vals);
        $mockmail->setField("id", $this->pdo->lastinsertid());
        return true;
    }

    public function updateMockMail(MailBase $mockmail)
    {

        list($fields, $legalfields) = $this->makeFieldsAndLegal($mockmail);

        list($settings, $vals) = $this->getSettingString($fields, $legalfields);
        $sql = "UPDATE mockmail SET {$settings} WHERE id=?";
        $vals[] = $mockmail->getId();

        $stmt = $this->pdo->prepare($sql);
        $stmt->execute($vals);
        return true;
    }
}
