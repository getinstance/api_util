<?php

/*
 * This file is part of the getinstance/api_util framework.
 *
 * (c)2018 getInstance Ltd <matt@getinstance.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace getinstance\api_util\persist;

use getinstance\api_util\command\FilterOption;
use InvalidArgumentException;
use PHPUnit\Util\Filter;

abstract class Mapper
{
    /**
     * Transforms a list of filter options into where clauses and SQL values.
     *
     * @param array<mixed> $options The parsed result from `Command:getFilterArray()`.
     * @param string       $prefix  If set, it will be prepended to each field name in the where clause.
     * @return string[][]   A tuple of where clauses and SQL values.
     */
    public function manageFilters(array $options, string $prefix = ''): array
    {
        $allWhere = [];
        $allVals = [];

        /** @var array<FilterOption> $filterOptions */
        $filterOptions = [];
        if (count($options) > 0) {
            if (isset($options[0]) && $options[0] instanceof FilterOption) {
                $filterOptions = $options;
            } else {
                // it's an old-style filter option format, convert to the new-style one
                foreach ($options as $key => $val) {
                    if (is_array($val)) {
                        foreach ($val as $operator => $innerVal) {
                            $filterOptions[] = new FilterOption($key, $operator, $innerVal);
                        }
                    } else {
                        $filterOptions[] = new FilterOption($key, '=', $val);
                    }
                }
            }
        }

        foreach ($filterOptions as $option) {
            list($where, $vals) = $this->handleFilter($option, $prefix);
            $allWhere[] = $where;
            $allVals = array_merge($allVals, $vals);
        }
        return [$allWhere, $allVals];
    }

    protected function getOrderString(array $fields, array $legalfields, string $tablequalifier = "")
    {
        $clauses = [];
        foreach ($fields as $key => $val) {
            if (is_numeric($key)) {
                $field = $val;
                $dir = "ASC";
            } else {
                $field = $key;
                $dir = $val;
            }
            if (! preg_match("/^(ASC|DESC)$/i", $dir)) {
                throw new \Exception("direction can only be ASC or DESC");
            }
            if (! in_array($field, $legalfields)) {
                throw new \Exception("not legal: $field");
            }

            $clause = "{$field} {$dir}";
            if (! empty($tablequalifier)) {
                $clause = "{$tablequalifier}.$clause";
            }
            $clauses[] = $clause;
        }
        return implode(",", $clauses);
    }

    protected function getSettingString(
        array $fields,
        array $legalfields,
        array $readonly = [],
        array $ignoreempty = []
    ): array {

        foreach ($legalfields as $key) {
            if (in_array($key, $readonly)) {
                continue;
            }
            $val = null;
            if (isset($fields[$key])) {
                $val = $fields[$key];
            }
            if (empty($val) && in_array($key, $ignoreempty)) {
                continue;
            }
            if ($key == "id") {
                continue;
            }
            $keys[] = $key;
            $vals[] = $val;
        }
        $settings = implode("=?, ", $keys) . "=?";
        return [$settings, $vals];
    }

    /**
     * Transforms a filter option into a where clause and SQL values.
     *
     * @param FilterOption|array $option The filter option to transform.
     * @param string       $prefix If set, it will be prepended to each field name in the where clause.
     *
     * @return array<string|array<string>> A tuple of where clause and SQL values.
     */
    private function handleFilter(FilterOption $option, string $prefix = ''): array
    {
        if (!in_array($option->field, $this->getValidFilterFields())) {
            throw new InvalidArgumentException("Unknown field name: {$option->field}");
        }

        $field = $prefix === '' ? $option->field : "{$prefix}.{$option->field}";

        switch ($option->operator) {
            case '~':
                $where = "$field LIKE ?";
                $vals = ["%{$option->value}%"];
                break;
            case 'IN':
                $vals = explode(',', $option->value);
                $where = "$field IN (" . implode(',', array_fill(0, count($vals), '?')) . ")";
                break;
            case 'NOT IN':
                $vals = explode(',', $option->value);
                $where = "$field NOT IN (" . implode(',', array_fill(0, count($vals), '?')) . ")";
                break;
            default:
                $where = "$field {$option->operator} ?";
                $vals = [$option->value];
                break;
        }

        return [$where, $vals];
    }

    abstract public function getValidFilterFields();
}
