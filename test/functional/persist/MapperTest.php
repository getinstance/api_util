<?php

declare(strict_types=1);

namespace getinstance\api_util\tests\persist;

use getinstance\api_util\command\FilterOption;
use getinstance\api_util\persist\Mapper;
use InvalidArgumentException;
use PHPUnit\Framework\TestCase;

class MapperTest extends TestCase
{
    public function testManageFiltersOldStyle(): void
    {
        $mapper = $this->createMapper();
        $options = [
            'id' => '1',
            'field_a' => ['NOT IN' => 'a,b,c'],
            'field_b' => ['~' => 'abc'],
        ];

        list($wheres, $vals) = $mapper->manageFilters($options, 't');
        $this->assertCount(3, $wheres);
        $this->assertEquals('t.id = ?', $wheres[0]);
        $this->assertEquals('t.field_a NOT IN (?,?,?)', $wheres[1]);
        $this->assertEquals('t.field_b LIKE ?', $wheres[2]);
        $this->assertCount(3, $vals);
        $this->assertEquals('1', $vals[0]);
        $this->assertEquals('["a","b","c"]', json_encode($vals[1]));
        $this->assertEquals('%abc%', $vals[2]);
    }

    public function testManageFiltersNewStyle(): void
    {
        $mapper = $this->createMapper();
        $options = [];
        $options[] = FilterOption::parse('id=1');
        $options[] = FilterOption::parse('field_a NOT IN a,b,c');
        $options[] = FilterOption::parse('field_b~abc');
        $options[] = FilterOption::parse('field_b~def');

        list($wheres, $vals) = $mapper->manageFilters($options, 't');
        $this->assertCount(4, $wheres);
        $this->assertEquals('t.id = ?', $wheres[0]);
        $this->assertEquals('t.field_a NOT IN (?,?,?)', $wheres[1]);
        $this->assertEquals('t.field_b LIKE ?', $wheres[2]);
        $this->assertEquals('t.field_b LIKE ?', $wheres[3]);
        $this->assertCount(4, $vals);
        $this->assertEquals('1', $vals[0]);
        $this->assertEquals('["a","b","c"]', json_encode($vals[1]));
        $this->assertEquals('%abc%', $vals[2]);
        $this->assertEquals('%def%', $vals[3]);
    }

    public function testManageFiltersException(): void
    {
        static::expectException(InvalidArgumentException::class);
        static::expectExceptionMessage('Unknown field name: field_c');
        $mapper = $this->createMapper();
        $options = [];
        $options[] = FilterOption::parse('field_c=1');
        $mapper->manageFilters($options);
    }

    private function createMapper(): Mapper
    {
        return new class extends Mapper {
            public function getValidFilterFields()
            {
                return ['id', 'field_a', 'field_b'];
            }
        };
    }
}
