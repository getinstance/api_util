<?php

declare(strict_types=1);

namespace getinstance\api_util\tests\command;

use getinstance\api_util\command\Command;
use getinstance\api_util\controller\Conf;
use getinstance\api_util\util\PubSubManager;
use PHPUnit\Framework\TestCase;
use Psr\Http\Message\ServerRequestInterface as Request;

class CommandTest extends TestCase
{
    public function testGetFilterArrayOldStyle(): void
    {
        $cmd = $this->createCommand();

        /** @var \PHPUnit\Framework\MockObject\Stub&Request $request */
        $request = $this->createStub(Request::class);
        $request->method('getQueryParams')
            ->willReturn(['filter' => 'abc=def;ghi~jkl']);
        $request->method('getParsedBody')
            ->willReturn([]);
        $filter = $cmd->getFilterArray($request);

        $expected = [
            'abc' => 'def',
            'ghi' => ['~' => 'jkl'],
        ];
        static::assertEquals(json_encode($expected), json_encode($filter));

        /** @var \PHPUnit\Framework\MockObject\Stub&Request $request */
        $request = $this->createStub(Request::class);
        $request->method('getQueryParams')
            ->willReturn(['filter' => 'fruit=apple;fruit=banana', 'filtermode' => 'OR']);
        $request->method('getParsedBody')
            ->willReturn([]);
        $filter = $cmd->getFilterArray($request);

        $expected = ['fruit' => 'banana'];
        static::assertEquals(json_encode($expected), json_encode($filter));
    }

    public function testGetFilterArrayNewStyle(): void
    {
        $cmd = $this->createCommand();

        /** @var \PHPUnit\Framework\MockObject\Stub&Request $request */
        $request = $this->createStub(Request::class);
        $request->method('getQueryParams')
            ->willReturn(['filter' => 'abc=def;ghi~jkl']);
        $request->method('getParsedBody')
            ->willReturn([]);
        $filter = $cmd->getFilterArray($request, true);

        $expected = [
            ['field' => 'abc', 'operator' => '=', 'value' => 'def'],
            ['field' => 'ghi', 'operator' => '~', 'value' => 'jkl'],
        ];
        static::assertEquals(json_encode($expected), json_encode($filter));

        /** @var \PHPUnit\Framework\MockObject\Stub&Request $request */
        $request = $this->createStub(Request::class);
        $request->method('getQueryParams')
            ->willReturn(['filter' => 'fruit=apple;fruit=banana', 'filtermode' => 'OR']);
        $request->method('getParsedBody')
            ->willReturn([]);
        $filter = $cmd->getFilterArray($request, true);

        $expected = [
            ['field' => 'fruit', 'operator' => '=', 'value' => 'apple'],
            ['field' => 'fruit', 'operator' => '=', 'value' => 'banana'],
        ];
        static::assertEquals(json_encode($expected), json_encode($filter));
    }

    public function testGetFilterArrayWithEmptyQs(): void
    {
        $cmd = $this->createCommand();

        /** @var \PHPUnit\Framework\MockObject\Stub&Request $request */
        $request = $this->createStub(Request::class);
        $request->method('getQueryParams')
            ->willReturn([]);
        $request->method('getParsedBody')
            ->willReturn([]);
        $filter = $cmd->getFilterArray($request, true);

        static::assertEquals('[]', json_encode($filter));
    }

    private function createCommand(): Command
    {
        $conf = new Conf('test');
        $pubSubManager = new PubSubManager();
        return new class($conf, $pubSubManager) extends Command {
        };
    }
}
