<?php

declare(strict_types=1);

namespace getinstance\api_util\tests\command;

use getinstance\api_util\command\FilterOption;
use InvalidArgumentException;
use PHPUnit\Framework\TestCase;

class FilterOptionTest extends TestCase
{
    public function testConstructorException(): void
    {
        static::expectException(InvalidArgumentException::class);
        static::expectExceptionMessage('Invalid operator: ><');
        new FilterOption('abc', '><', 'def');
    }

    public function testParseException(): void
    {
        static::expectException(InvalidArgumentException::class);
        static::expectExceptionMessage('Unable to parse: abc');
        FilterOption::parse('abc');
    }
}
