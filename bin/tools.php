<?php
$toplevel = __DIR__.'/../../../../';
require_once "{$toplevel}/vendor/autoload.php"; 

use Aura\Cli\CliFactory;

$clifactory = new CliFactory;
$context = $clifactory->newContext($GLOBALS);
$stdio = $clifactory->newStdio();

$cmd = $context->argv->get(1);
$pkg = "\\getinstance\\api_util\\clicommand\\";
$defaultcmd = "default";

$acceptable = [
    "default" => "{$pkg}DefaultCmd",
    "genendpoint" => "{$pkg}GenEndpointCmd",
    "genweb" => "{$pkg}GenWebCmd",
];

if (empty($cmd)) {
    $cmd = $defaultcmd;
}
if (! isset($acceptable[$cmd])) {
    throw new \Exception("unknown state command: $cmd");
}

$cmdobj = new $acceptable[$cmd]();
$cmdobj->execute($context, $stdio);
